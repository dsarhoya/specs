Pod::Spec.new do |s|
  s.name         = "ColumnsLabel"
  s.version      = "0.0.5"
  s.summary      = "A subclass of UIView, to simulate a UILabel that distributes the text into multiple columns"
  s.homepage     = "https://github.com/amcastror/ColumnsLabel"
  s.license      = 'MIT'
  s.author       = { "Matias Castro" => "mattias.castro@gmail.com" }
  s.source       = { :git => "https://github.com/amcastror/ColumnsLabel.git", :tag => '0.0.5' }
  s.platform     = :ios, '5.0'
  s.ios.deployment_target = '5.0'
  s.requires_arc = true
  s.source_files = 'Classes/ios/*.{h,m}'
end
