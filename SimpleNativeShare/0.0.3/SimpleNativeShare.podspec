Pod::Spec.new do |s|
  s.name         = "SimpleNativeShare"
  s.version      = "0.0.3"
  s.summary      = "Native share dialog with default behaviour"
  s.homepage     = "https://bitbucket.org/dsarhoya/dsy-ios-simple-native-share"
  s.license      = 'MIT'
  s.author       = { "Matias Castro" => "matias.castro@dsarhoya.cl" }
  s.source       = { :git => "https://bitbucket.org/dsarhoya/dsy-ios-simple-native-share.git", :tag => '0.0.3' }
  s.platform     = :ios, '6.0'
  s.ios.deployment_target = '6.0'
  s.requires_arc = true
  s.source_files = 'SimpleNativeShare/SimpleNativeShare/*.{h,m}'
  s.dependency 'DejalActivityView', '1.2'
end
