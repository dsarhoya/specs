Pod::Spec.new do |s|
  s.name         = "SimpleImagePicker"
  s.version      = "0.0.5"
  s.summary      = "Image picker with default behaviour"
  s.homepage     = "https://bitbucket.org/dsarhoya/dsy-ios-simple-image-picker"
  s.license      = 'MIT'
  s.author       = { "Matias Castro" => "matias.castro@dsarhoya.cl" }
  s.source       = { :git => "git@bitbucket.org:dsarhoya/dsy-ios-simple-image-picker.git", :tag => '0.0.5' }
  s.platform     = :ios, '6.0'
  s.ios.deployment_target = '6.0'
  s.requires_arc = true
  s.source_files = 'SimpleImagePicker/SimpleImagePicker/*.{h,m}'
  s.ios.resource_bundles = {"SimpleImagePicker-iOS"=>["SimpleImagePicker/SimpleImagePicker/*.lproj"]}
  s.dependency 'DSYUtils', '0.0.2'
end
