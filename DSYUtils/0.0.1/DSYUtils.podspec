Pod::Spec.new do |s|
  s.name         = "DSYUtils"
  s.version      = "0.0.1"
  s.summary      = "A library for common solutions"
  s.homepage     = "https://github.com/amcastror/DSYUtils"
  s.license      = 'MIT'
  s.author       = { "Matias Castro" => "mattias.castro@gmail.com" }
  s.source       = { :git => "https://github.com/amcastror/DSYUtils.git", :tag => '0.0.1' }
  s.platform     = :ios, '5.0'
  s.ios.deployment_target = '5.0'
  s.requires_arc = true
  s.source_files = 'DSYUtils/DSYUtils/*.{h,m}'
end
