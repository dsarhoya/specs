Pod::Spec.new do |s|
  # s.name         = "DSYCustomEurekaRow"
  # s.version      = "0.0.1"
  # s.summary      = "A library for common solutions"
  # s.homepage     = "https://github.com/amcastror/DSYUtils"
  s.license        = 'MIT'
  # s.author       = { "Matias Castro" => "mattias.castro@gmail.com" }
  # s.source       = { :git => "https://github.com/amcastror/DSYUtils.git", :tag => '0.0.1' }
  # s.platform     = :ios, '5.0'
  # s.ios.deployment_target = '5.0'
  # s.requires_arc = true
  # s.source_files = 'DSYUtils/DSYUtils/*.{h,m}'
  
  s.platform = :ios
  s.ios.deployment_target = '8.0'
  s.name         = "DSYCustomEurekaRow"
  s.summary = "DSYCustomEurekaRows is a framework that generates a presenter row from your custom cell and your custom view controller"
  s.requires_arc = true

  # 2
  s.version      = "0.0.2"

  # 3
  # s.licence = { :type => "MIT", :file => "LICENSE" }

  # 4
  s.author = { "dsarhoya " => "contacto@dsy.cl" }

  # 5
  s.homepage = "https://bitbucket.org/dsarhoya/dsy-custom-eureka-rows"

  # 6
  s.source = { :git => "git@bitbucket.org:dsarhoya/dsy-custom-eureka-rows.git", :tag => "0.0.2" }

  # 7
  s.ios.frameworks = 'UIKit', 'Foundation'
  s.dependency 'Eureka', '3.0'

  # 8
  s.ios.source_files = 'Sources/**/*.swift'
  
  #9
  #s.ios.resource_bundle = {"DSYCustomEurekaRow" => 'Sources/Media.xcassets'}
  s.resources = "Sources/**/*.xcassets"

end
